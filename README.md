Wechselt im OXID eShop 7 EE die Template Engine von Twig zu Smarty zur Nutzung smarty-basierter Themes

## Installation

```
composer require d3/oxid-smarty-renderer-metapackage-ee --update-no-dev
```

Installiere im Anschluss Dein gewünschtes smarty-basiertes Frontendtheme (z.B. Wave oder Flow).

Leere den TMP-Ordner.

Aktiviere im Admin das smarty-basierte Frontend-Theme.

Beachte bitte:
- twig-basierte Frontendthemes werden nicht entfernt, verursachen bei Aktivierung jedoch Fehler im Shop
- die Templateengine kann nur systemweit umgestellt werden. Auch das Backend verwendet bei der Umstellung die geänderte Templateengine. Für den Admin wird automatisch ein entsprechendes Theme verwendet.

Da bei Modulinstallation im Twig Renderer Kontext keine Smarty-Templates und -Plugins registriert werden, müssen nach Umstellung des Renderers alle Plugins mit entsprechenden Einträge neu aktiviert werden.

## Deinstallation

Führen den folgenden Befehl aus, um zum Standard (Twig) zurück zu wechseln.

```
composer remove d3/oxid-smarty-renderer-metapackage-ee d3/oxid-smarty-renderer-metapackage-pe d3/oxid-smarty-renderer-metapackage-ce --update-no-dev
```

Leere den TMP-Ordner.

Aktiviere im Admin das twig-basierte Frontend-Theme.